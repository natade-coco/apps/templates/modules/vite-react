# Vite React

vite-reactを使用したクライアントアプリ

## Develop

#### 1. 開発用サーバーアプリを立ち上げる

#### 2. `client` フォルダ直下に `.env.development.local` ファイルを作成し、以下の環境変数を設定する

```
VITE_API_URL=http://localhost:3030  # 開発用サーバーアプリのエンドポイント(必須)
VITE_APP_TOKEN=xxx                  # Pocket(モバイルアプリ)の認証JWT
```

#### 3. `$ npm install && npm start` => `localhost:5173` へアクセス
