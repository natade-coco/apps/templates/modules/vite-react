import { useMemo } from 'react'

const mapError = (error: Error): string => {
  switch (error.message) {
  case 'server':
    return '[Server Error] サーバーエラーが発生しました。'
  case 'unsupported':
    return '[Unsupported] この端末ではコンテンツをご利用いただけません。'
  case 'internal':
    return '[Internal Error] 内部エラーが発生しました。'
  default:
    return '[Unknown Error] 不明なエラーが発生しました。'
  }
}

type Props = {
  error: Error | undefined
}

const ErrorMessage = ({ error }: Props) => {
  const errorMessage = useMemo(() => (
    error ? mapError(error) : undefined
  ), [error])

  return error ? (
    <div className='notification is-danger mx-3'>
      {errorMessage}
    </div>
  ) : (
    <div />
  )
}

export default ErrorMessage
