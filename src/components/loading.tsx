import { useState, useEffect, useRef } from 'react'
import { BeatLoader } from 'react-spinners'
import { useSelector } from 'react-redux'

import { isLoadingSelector } from 'slices/app'

const Loading = () => {
  const isLoading = useSelector(isLoadingSelector)

  const [showsModal, setShowsModal] = useState(false)
  const timeoutId = useRef<NodeJS.Timeout>()

  useEffect(() => {
    if (isLoading) {
      if (timeoutId.current) {
        clearTimeout(timeoutId.current)
      }
      setShowsModal(true)
    } else {
      timeoutId.current = setTimeout(() => {
        setShowsModal(false)
      }, 300)
    }
  }, [isLoading])

  return (
    <LoadingModal showsModal={showsModal} />
  )
}

type LoadingModalProps = {
  showsModal: boolean
}

export const LoadingModal = (props: LoadingModalProps) => (
  <div className={`modal ${props.showsModal && 'is-active'}`}>
    <div className="modal-background" />
    <BeatLoader size={15} margin={4} color="rgba(0,0,0,0.5)" />
  </div>
)

export default Loading
