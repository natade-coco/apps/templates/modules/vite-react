import { Routes, Route } from 'react-router-dom'

import MainScreen from 'screens/main'

const Router = () => (
  <Routes>
    <Route path="/" element={<MainScreen />} />
  </Routes>
)

export default Router
