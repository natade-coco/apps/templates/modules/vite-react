import { useCallback, useEffect, useMemo } from 'react'
import Modal from 'react-modal'

import './select-modal.css'

const modalStyle: Modal.Styles = {
  content: {
    width: '50%',
    left: '25%',
    padding: '.5rem'
  },
  overlay: {
    transition: 'opacity 200ms ease-in-out'
  }
}

export type Item = {
  value: string,
  label: string
}

const makeId = (item: Item) => `${item.value}_${item.label}`

type Props = {
  isActive: boolean,
  items: Item[],
  activeItem: Item | undefined,
  onSelected: (item: Item) => void,
  onCloseRequested: () => void
}
const SelectModal = (props: Props) => {
  const { isActive, items, activeItem, onSelected, onCloseRequested } = props

  const activeId = useMemo(() => (
    activeItem && makeId(activeItem)
  ), [activeItem])

  const onItemPressed = useCallback((item: Item) => {
    onSelected(item)
    onCloseRequested()
  }, [])

  const onOpened = useCallback(() => {
    if (activeId) {
      const element = document.getElementById(activeId)
      element?.scrollIntoView()
    }
  }, [activeId])

  return (
    <Modal
      style={modalStyle}
      isOpen={isActive}
      closeTimeoutMS={200}
      onAfterOpen={onOpened}
      onRequestClose={onCloseRequested}
    >
      <div className="select-modal">
        <div>
          {items.map((item) => {
            const id = makeId(item)
            return (
              <div
                key={id}
                id={id}
                className={`item has-text-centered ${(id === activeId) ? 'active' : ''}`}
                onClick={() => onItemPressed(item)}
              >
                {item.label}
              </div>
            )
          })}
        </div>
      </div>
    </Modal>
  )
}

export default SelectModal
