import Pocket from '@natade-coco/pocket-sdk'

export default class PocketService {
  requestSignJWT = async (): Promise<string> => {
    const devToken = import.meta.env.VITE_APP_TOKEN
    if (devToken) {
      console.log('[requestSignJWT]', 'development mode')
      return devToken
    } else {
      try {
        const jwt = await Pocket.requestSignJWT()
        return jwt
      } catch (err) {
        console.error('[requestSignJWT] err', err)
        throw new Error('internal')
      }
    }
  }
}
