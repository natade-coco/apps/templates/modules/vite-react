import { Sample } from 'models/sample'

export default interface SampleService {
  getSamples: () => Promise<Sample[]>
}
