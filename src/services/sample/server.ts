import { Application, FeathersService, Service } from '@feathersjs/feathers'

import { Sample } from 'models/sample'
import { serverApp } from 'services/server'
import SampleService from './index'

export default class ServerSampleService implements SampleService {
  feathers: {
    sample: FeathersService<Application, Service<any>>
  }

  constructor() {
    this.feathers = {
      sample: serverApp.service('samples')
    }
  }

  getSamples = async (): Promise<Sample[]> => {
    try {
      const response: Sample[] = await this.feathers.sample.find()
      return response
    } catch (error) {
      throw error
    }
  }
}
