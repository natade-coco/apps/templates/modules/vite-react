import { Session } from 'models/app'

export default interface AppService {
  getSession: (jwt?: string) => Promise<Session>
}
