import { Session } from 'models/app'

import AppService from './index'
import { serverApp } from 'services/server'

export default class ServerAppService implements AppService {
  getSession = async (jwt?: string): Promise<Session> => {
    try {
      const session = serverApp?.service('authentication').create({ strategy: 'natadecoco', token: jwt })
      return session
    } catch (err) {
      console.error('[getSession] err', err)
      throw new Error('server')
    }
  }
}
