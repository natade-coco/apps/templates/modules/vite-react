import { PersistedState } from 'redux-persist'

export const STORE_VERSION = 1

export default {
  [`${STORE_VERSION}`]: (state: PersistedState) => {
    return {}
  }
}
