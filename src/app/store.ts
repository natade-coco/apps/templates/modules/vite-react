import { configureStore, Action } from '@reduxjs/toolkit'
import { ThunkAction } from 'redux-thunk'
import { persistReducer, createMigrate, PERSIST } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import rootReducer, { RootState } from './root-reducer'
import migration, { STORE_VERSION } from './migration'

const persistedReducer = persistReducer({
  key: 'root',
  storage,
  whitelist: ['sample'],
  version: STORE_VERSION,
  // @ts-ignore
  migrate: createMigrate(migration)
}, rootReducer)

const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware => (
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [PERSIST]
      }
    })
  )
})

export type AppDispatch = typeof store.dispatch
export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>

export default store
