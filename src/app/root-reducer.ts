import { combineReducers } from '@reduxjs/toolkit'

import AppReducer from 'slices/app'
import SampleReducer from 'slices/sample'

const rootReducer = combineReducers({
  app: AppReducer,
  sample: SampleReducer
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
