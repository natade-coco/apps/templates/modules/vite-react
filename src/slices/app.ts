import { createSlice, PayloadAction, createSelector, createAsyncThunk,
         isPending, isRejected, isFulfilled } from '@reduxjs/toolkit'

import { RootState } from 'app/root-reducer'
import { Session } from 'models/app'
import PocketService from 'services/pocket'
import AppService from 'services/app'
import ServerAppService from 'services/app/server'

const appService: AppService = new ServerAppService()
const pocket = new PocketService()

export const LOCAL_USER = 'local-user'

export interface State {
  pendingActions: string[],
  session: Session | undefined
}

const initialState: State = {
  pendingActions: [],
  session: undefined
}

// Selectors

const inputSelector = (state: RootState) => state.app

export const isLoadingSelector = createSelector(
  inputSelector,
  (state: State) => state.pendingActions.length > 0
)

export const sessionSelector = createSelector(
  inputSelector,
  (state: State) => state.session
)

// Thunks

export type AppStartResponse = {
  session: Session
}
export const appStart = createAsyncThunk(
  'app/start',
  async (): Promise<AppStartResponse> => {
    try {
      const jwt = await pocket.requestSignJWT()
      const session = await appService.getSession(jwt)
      return { session }
    } catch (err) {
      throw err
    }
  }
)

// Slice

const getOriginalActionType = (action: PayloadAction<any>): string =>
  action.type.slice(0, action.type.lastIndexOf('/'))

const startLoading = (state: State, action: PayloadAction<any>) => {
  const set = new Set(state.pendingActions)
  set.add(getOriginalActionType(action))
  state.pendingActions = Array.from(set)
}

const loadingSuccess = (state: State, action: PayloadAction<any>) => {
  const set = new Set(state.pendingActions)
  set.delete(getOriginalActionType(action))
  state.pendingActions = Array.from(set)
}

const loadingFailed = (
  state: State,
  action: PayloadAction<any, string, any, any>
) => {
  const set = new Set(state.pendingActions)
  set.delete(getOriginalActionType(action))
  state.pendingActions = Array.from(set)
  console.error(`[loadingFailed]`, JSON.stringify(action.error?.message))
}

const slice = createSlice({
  name: 'app',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(
        appStart.fulfilled,
        (state, { payload }: PayloadAction<AppStartResponse>) => {
          state.session = payload.session
        }
      )
      .addMatcher(isPending, startLoading)
      .addMatcher(isFulfilled, loadingSuccess)
      .addMatcher(isRejected, loadingFailed)
  },
})

export default slice.reducer
