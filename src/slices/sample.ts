import { createSlice, createAsyncThunk, PayloadAction, createEntityAdapter } from '@reduxjs/toolkit'

import { RootState } from 'app/root-reducer'
import { Sample } from 'models/sample'
import SampleService from 'services/sample'
import ServerSampleService from 'services/sample/server'

const sampleService: SampleService = new ServerSampleService()

// Entity Adapter

const entityAdapter = createEntityAdapter<Sample>()

const initialState = entityAdapter.getInitialState()

// Selectors

const inputSelector = (state: RootState) => state.sample

export const sampleSelectors = entityAdapter.getSelectors(inputSelector)

// Thunks

export type GetSampleResponse = {
  samples: Sample[]
}
export const getSamples = createAsyncThunk(
  'sample/getSamples',
  async (_, { getState }): Promise<GetSampleResponse> => {
    const state = getState() as RootState
    const total = sampleSelectors.selectTotal(state)
    const samples = await sampleService.getSamples()
    return { samples }
  }
)

// Slice

const slice = createSlice({
  name: 'sample',
  initialState,
  reducers: {},
  extraReducers: builder => builder
    .addCase(
      getSamples.fulfilled,
      (state, { payload }: PayloadAction<GetSampleResponse>) => {
        entityAdapter.setAll(state, payload.samples)
      }
    )
})

export default slice.reducer
