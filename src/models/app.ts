interface User {
  id: string
}

export interface Session {
  accessToken: string,
  user: User
}

export type Id = string | number
