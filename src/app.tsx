import { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'

import { AppDispatch } from 'app/store'
import Layout from 'components/layout'
import Router from 'components/router'
import { LoadingModal } from 'components/loading'
import { appStart } from 'slices/app'

const App = () => {
  const dispatch: AppDispatch = useDispatch()

  const [isAppReady, setIsAppReady] = useState(false)

  useEffect(() => {
    dispatch(appStart())
      .unwrap()
      .then(() => setIsAppReady(true))
  }, [])

  if (!isAppReady) return (
    <LoadingModal showsModal />
  )

  return (
    <Layout>
      <Router />
    </Layout>
  )
}

export default App
