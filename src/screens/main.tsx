import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import './main.css'
import { AppDispatch } from 'app/store'
import { getSamples, sampleSelectors } from 'slices/sample'

type Props = {}

const MainScreen = (props: Props) => {
  const dispatch: AppDispatch = useDispatch()

  const samples = useSelector(sampleSelectors.selectAll)

  useEffect(() => {
    dispatch(getSamples())
  }, [])

  return <>
    <div className="main">
      {samples.map(sample => (
        <div key={sample.id}>{sample.id}: {sample.name}</div>
      ))}
    </div>
  </>
}

export default MainScreen
